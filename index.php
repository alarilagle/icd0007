<?php
    require_once "lib/tpl.php";
    require_once "functions.php";

    $cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "mainPage";

    if ($cmd == "mainPage") {
        print render_template("tpl/mainPage.html", ['$persons' => get_persons()]);

    } else if ($cmd == "add") {
        print render_template("tpl/additionPage.html");

    } else if ($cmd == "submit-button") {

        if (isset($_POST["firstName"]) && isset($_POST["lastName"])) {
            $firstName = $_POST["firstName"];
            $last_name = $_POST["lastName"];
            $phones = [$_POST["phone1"], $_POST["phone2"], $_POST["phone3"]];
            $id = $_POST["id"];
            //$phone = $_POST["phone"];
            $errors = validate_inputs($firstName, $last_name);
            $person = new Person($firstName, $last_name, $phones);
            if (count($errors) == 0) {
                if (strlen($_POST["id"] == 0)) {
                    add_person($person);
                } else {
                    update_person($firstName, $last_name, $id);
                    delete_numbers_from_database($id);
                    add_number($phones, $id);
                }
                header("Location: ?cmd=mainPage");
            } else {
                print render_template("tpl/additionPage.html", ['$errors' => $errors, '$person' => $person]);
            }
            //add_data($firstName, $lastName, $phone);
        }
    } else if (explode(";", $cmd)[0] == "edit") {
        $data = explode(";", $cmd);
        $id = $data[1];
        $person = find_person_by_id($id, get_persons());
        print render_template("tpl/additionPage.html", ['$person' => $person]);
    }
