<?php
/**
 * Created by PhpStorm.
 * User: alarilagle
 * Date: 24/03/2018
 * Time: 10:55
 */

require_once "Person.php";
const DATA_FILE = "data.txt";

function add_person($person) {
    //if (isset($first_name) && $first_name !== "" && isset($last_name) && $last_name !== ""
      //  && isset($number) && $number !== "") {
      //  file_put_contents(DATA_FILE, "$first_name ; $last_name ; $number" . PHP_EOL, FILE_APPEND);
    //}
    $connection = new PDO("sqlite:data.sqlite");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("insert into persons (first_name, last_name) VALUES (:first, :last)");
    $statement->bindValue(":first", $person->firstName);
    $statement->bindValue(":last", $person->lastName);

    $statement->execute();
    $last_id = $connection->lastInsertId();

    add_number($person->phones, $last_id);
}

function add_number($phones, $id) {
    foreach ($phones as $phone) {

        if ($phone != null) {
            $connection = new PDO("sqlite:data.sqlite");
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $statement = $connection->prepare("insert into phones (phone, person_id) VALUES (:phone, :person_id)");
            $statement->bindValue(":phone", $phone);
            $statement->bindValue(":person_id", $id);

            $statement->execute();
        }
    }
}

//function get_data() {
  //  return file(DATA_FILE);
//}

function get_persons() {
    /*
    $persons = [];
    $connection = new PDO("sqlite:data.sqlite");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("SELECT person.id, person.first_name, person.last_name, 
    GROUP_CONCAT(phones.phone) AS numbers FROM persons AS person INNER JOIN phones ON phones.person_id = person.id 
    GROUP BY person.id, person.first_name, person.last_name;");
    $statement->execute();

    foreach ($statement as $person) {
        //$numbers = explode(",", $person["numbers"]);
        $persons[] = new Person($person["first_name"], $person["last_name"], $person["numbers"], $person["id"]);
    }
    return $persons;
    */
    $persons = [];
    $connection = new PDO("sqlite:data.sqlite");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("select * from persons");
    $statement->execute();

    foreach ($statement as $person) {
        $statement2 = $connection->prepare("select phone from phones WHERE $person[id] = phones.person_id");
        $statement2->execute();
        $numbers = [];
        foreach ($statement2 as $number) {
            $numbers[] = $number["phone"];
        }
        $persons[] = new Person($person["first_name"], $person["last_name"], $numbers, $person['id']);
    }

    return $persons;

    /*
    $persons = [];
    foreach (get_data() as $line) {
        $data = explode(';', trim($line));
        list($firstName, $lastName, $number1, $number2, $number3) = $data;
        $persons[] = new Person($firstName, $lastName, $number1, $number2, $number3);
    }ei
    return $persons;
    */
}

function validate_inputs($first_name, $last_name) {
    $errors = [];

    if (strlen($first_name) < 2) {
        $errors[] = "Eesnimi peab olema pikem kui 2 tähemärki!";
    }
    if (strlen($last_name) < 2) {
        $errors[] = "Perekonnanimi peab olema pikem kui 2 tähemärki!";
    }
    return $errors;
}

function find_person_by_id($id, $persons) {
    foreach ($persons as $person) {
        if ($person->id == $id) {
            //$numbers = explode(",", $person->phones);
            $person->setPhones($person->phones);
            return $person;
        }
    }
}

function delete_numbers_from_database($id) {
    $connection = new PDO("sqlite:data.sqlite");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("DELETE FROM phones WHERE person_id=:id");
    $statement->bindValue(":id", $id);
    $statement->execute();
}

function update_person($first_name, $last_name, $id) {
    $connection = new PDO("sqlite:data.sqlite");
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $statement = $connection->prepare("UPDATE persons SET first_name = :first_name, last_name = :last_name WHERE ID = $id");
    $statement->bindValue(":first_name", $first_name);
    $statement->bindValue(":last_name", $last_name);

    $statement->execute();
}
