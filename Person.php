<?php
/**
 * Created by PhpStorm.
 * User: alarilagle
 * Date: 25/04/2018
 * Time: 14:08
 */

class Person {

    public $firstName;
    public $lastName;
    public $phones;
    public $id;

    public function __construct($firstName, $lastName, $phones, $id=null) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->phones = $phones;
        $this->id = $id;
    }

    /**
     * @param mixed $phones
     */
    public function setPhones($phones) {
        $this->phones = $phones;
    }

}
